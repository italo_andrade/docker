FROM torizon/debian:1-buster
WORKDIR /home/torizon

RUN apt-get -y update && apt-get install -y \
    python3 python3-dev libatlas-base-dev \
    cmake build-essential gcc g++ git \
    && apt-get clean && apt-get autoremove

RUN apt-get install -y python3 python3-wheel python3-pil python3-numpy python3-setuptools \
    && apt-get clean && apt-get autoremove

#### INSTALL GSTREAMER ####
RUN apt-get -y update && apt-get install --no-install-recommends -y \
    libgstreamer1.0-0 gstreamer1.0-plugins-base \
    gstreamer1.0-plugins-good gstreamer1.0-plugins-bad gstreamer1.0-plugins-ugly \
    gstreamer1.0-tools libdrm2 \
    v4l-utils libgstreamer1.0-dev \ 
    libgstreamer-plugins-base1.0-dev \
    && apt-get clean && apt-get autoremove

RUN apt-get -y update && apt-get install -y \
    python3-opencv \
    && apt-get clean && apt-get autoremove

RUN apt-get -y update && apt-get install -y \
    gpiod \
    && apt-get clean && apt-get autoremove && rm -rf /var/lib/apt/lists/*

# Allow the user torizon use GPIOs
RUN usermod -a -G gpio torizon

# RUN git clone https://github.com/opencv/opencv.git
# RUN cd opencv && git checkout 4.2.0 && mkdir -p build && cd build && cmake \
#   -DWITH_OPENGL=ON -DWITH_TBB=ON -DWITH_PTHREADS_PF=ON  \
#   -DCMAKE_BUILD_TYPE=RELEASE -DWITH_GSTREAMER=ON -DVIDEOIO_PLUGIN_LIST=gstreamer -DWITH_OPENCL=ON -DWITH_CSTRIPES=ON \
#   -DWITH_VULKAN=ON -DENABLE_PRECOMPILED_HEADERS=OFF \
#     -DWITH_OPENVX=ON -DWITH_V4L=ON -DWITH_LIBV4L=ON \
#     -DENABLE_NEON=ON -DENABLE_TBB=ON-DENABLE_IPP=ON -DENABLE_VFVP3=ON \
#   ../ && make -j`nproc` && make install


RUN apt-get -y update && apt-get install -y python3-flask python3-pip nano

RUN pip3 install imutils